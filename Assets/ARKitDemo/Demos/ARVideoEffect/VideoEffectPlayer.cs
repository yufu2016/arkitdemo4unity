﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoEffectPlayer : MonoBehaviour {
	private VideoPlayer m_VideoPlayer;
	// Use this for initialization
	void Start () {
		m_VideoPlayer = GetComponent<VideoPlayer> ();
		#if UNITY_EDITOR
		var videoPath = Application.dataPath + "/StreamingAssets/Video01.MOV";
		#else 
		var videoPath = Application.dataPath + "/Raw/"+"Video01.MOV";
		#endif
		Debug.Log("path:  "+videoPath);
		m_VideoPlayer.url = videoPath;
		m_VideoPlayer.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
