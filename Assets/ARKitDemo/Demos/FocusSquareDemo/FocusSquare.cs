﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class FocusSquare : MonoBehaviour {

	void OnEnable(){
		HitTestManager.Instance.OnScreenCenterScan += HandleScreenCenterScan;
	}

	void OnDisable(){
		HitTestManager.Instance.OnScreenCenterScan -= HandleScreenCenterScan;
	}

	void OnDestroy(){
		HitTestManager.Instance.OnScreenCenterScan -= HandleScreenCenterScan;
	}

	void HandleScreenCenterScan(Vector3 position, Quaternion rotation){
		transform.position = position;
		transform.rotation = rotation;
	}
}
