﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.XR.iOS;

public class MeasureARKitController : MonoBehaviour {
	[SerializeField]private Text m_MeasureDataText;

	private Vector3 HitTestWithResultPosition (ARPoint point, ARHitTestResultType resultTypes)
	{
		List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface ().HitTest (point, resultTypes);
		if (hitResults.Count > 0) {
			foreach (var hitResult in hitResults) {
				var position = UnityARMatrixOps.GetPosition (hitResult.worldTransform);
				return position;
			}
		}
		return Vector3.zero;
	}

	private Vector3 beginPosition = Vector3.zero;
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetMouseButton (0)) {
//			var screenPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
//			ARPoint beginPoint = new ARPoint {
//				x = screenPosition.x,
//				y = screenPosition.y
//			};
//
//			beginPosition = HitTestWithResultPosition (beginPoint, ARHitTestResultType.ARHitTestResultTypeFeaturePoint);
//			Debug.Log ("beginPosition:   " + beginPosition);
//		}

		if (Input.touchCount > 0)
		{
			var touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Began)
			{
				var screenPosition = Camera.main.ScreenToViewportPoint(new Vector3(Screen.width/2,Screen.height/2,0));
				ARPoint beginPoint = new ARPoint {
					x = screenPosition.x,
					y = screenPosition.y
				};

				beginPosition = HitTestWithResultPosition (beginPoint, ARHitTestResultType.ARHitTestResultTypeFeaturePoint);
				Debug.Log ("beginPosition:   " + beginPosition);
			}

			if (touch.phase == TouchPhase.Moved) {
				
				var screenPosition = Camera.main.ScreenToViewportPoint(new Vector3(Screen.width/2,Screen.height/2,0));
				ARPoint MovedPoint = new ARPoint {
					x = screenPosition.x,
					y = screenPosition.y
				};

				var currentPisition = HitTestWithResultPosition (MovedPoint, ARHitTestResultType.ARHitTestResultTypeFeaturePoint);
				Debug.Log ("Vector3.Distance:   " + Vector3.Distance(currentPisition,beginPosition));
				var distance = Vector3.Distance (currentPisition, beginPosition);
				m_MeasureDataText.text = "测量中的距离： " + (distance*100f).ToString ()+" 厘米";

			}

			if (touch.phase == TouchPhase.Ended) {
				Debug.Log ("ending....:   " + beginPosition);

				var screenPosition = Camera.main.ScreenToViewportPoint(new Vector3(Screen.width/2,Screen.height/2,0));
				ARPoint EndedPoint = new ARPoint {
					x = screenPosition.x,
					y = screenPosition.y
				};

				var endPosition = HitTestWithResultPosition (EndedPoint, ARHitTestResultType.ARHitTestResultTypeFeaturePoint);
				Debug.Log ("Vector3.End...:   " + Vector3.Distance(endPosition,beginPosition));
				var distance = Vector3.Distance (endPosition, beginPosition);
				m_MeasureDataText.text = "最后的距离： " + (distance*100f).ToString ()+" 厘米";
			}
		}
	}
}
