﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitGenerialMissile : MonoBehaviour {

	[SerializeField]private Transform m_HitTransform;
	// Use this for initialization
	void Start () {
		HitTestManager.Instance.OnScreenHit += OnHitScreen;
	}

	void OnDisable(){
		HitTestManager.Instance.OnScreenHit -= OnHitScreen;
	}

	void OnHitScreen(Vector3 position, Quaternion rotation){
		//m_HitTransform.transform.position = position;
		//m_HitTransform.transform.rotation = rotation;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			OnHitScreen (Vector3.zero, Quaternion.identity);
		}
	}

	public void FireMissile(){
		ARMissil missile = m_HitTransform.GetComponent<ARMissil> ();
		missile.Fire ();
	}

	public void ReLoadScene(){
		UnityEngine.SceneManagement.SceneManager.LoadScene ("ARMissile");
	}


}
