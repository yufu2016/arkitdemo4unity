﻿using UnityEngine;
using System.Collections;

public class ARMissil : MonoBehaviour {

	public float velocidade = 100f;
	public float torque = 25f;
	public Rigidbody missilRb;
	public Transform alvo;
	public AudioSource nearAudioSource;


	void Start ()
	{
		missilRb = transform.GetComponent<Rigidbody> ();
		if (alvo == null) {
			alvo = GameObject.FindGameObjectsWithTag ("Player") [0].transform;
		}
		//StartCoroutine(Fire ());
	}
	void FixedUpdate()
	{
		if (alvo == null || missilRb == null)
			return;
		if (fire) {
			missilRb.velocity = transform.forward * velocidade;
			var targetRotation = Quaternion.LookRotation (alvo.position - transform.position);
			missilRb.MoveRotation (Quaternion.RotateTowards (transform.rotation, targetRotation, torque));
		}
	}
		

	bool fire =false;
	public void Fire()
	{
		//等待两面后发射
		fire = true;
//		float distancia = Mathf.Infinity;
//		Vector3 position = transform.position;
//		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))
//		{
//			Vector3 diff = go.transform.position - position;
//			float distanciaAtual = diff.sqrMagnitude;
//			if(distanciaAtual < distancia)
//			{
//				distancia = distanciaAtual;
//				alvo = go.transform;
//			}
//		}
	}

	void OnTriggerEnter(Collider other) {
		Debug.Log ("other"+other.name);
		nearAudioSource.Play ();
		StartCoroutine (Dead ());
	}

	IEnumerator Dead(){
		yield return new WaitForSeconds (1.7f);
		Destroy (transform.parent.gameObject);
	}
}
