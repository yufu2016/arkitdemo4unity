﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.iOS;

public class AnchorEventDemo : MonoBehaviour {

	[SerializeField] private Text m_AddTimesText;
	[SerializeField] private Text m_UpdateTimesText;
	[SerializeField] private Text m_RemoveTimesText;

	void OnEnable(){
		UnityARSessionNativeInterface.ARAnchorAddedEvent += ARAnchorAdded;
		UnityARSessionNativeInterface.ARAnchorUpdatedEvent += ARAnchorUpdated;
		UnityARSessionNativeInterface.ARAnchorRemovedEvent += ARAnchorRemoved;

		UnityARSessionNativeInterface.ARFrameUpdatedEvent += ARFrameUpdated;

	}
	void OnDisable(){
		
	}
	void OnDestroy(){
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	//event handle function

	//add the Anchor
	private int m_AddTimescount = 0;
	void ARAnchorAdded(ARPlaneAnchor anchorData){
		print("ARPlaneAnchor added");
		m_AddTimescount++;
		m_AddTimesText.text = "Anchor added " + m_AddTimescount.ToString () + " times";

	}

	//update the Anchor

	private int m_UpdateTimescount = 0;
	void ARAnchorUpdated(ARPlaneAnchor anchorData){
		print("ARPlaneAnchor updated");
		m_UpdateTimescount++;
		m_UpdateTimesText.text = "Anchor added " + m_UpdateTimescount.ToString () + " times";
	}

	private int m_RemoveTimescount = 0;
	void  ARAnchorRemoved(ARPlaneAnchor anchorData){
		print("ARPlaneAnchor removed");
		m_RemoveTimescount++;
		m_RemoveTimesText.text = "Anchor added " + m_RemoveTimescount.ToString () + " times";
	}
	void ARFrameUpdated(UnityARCamera camera){
		print("AR camera updating");
	}
}
