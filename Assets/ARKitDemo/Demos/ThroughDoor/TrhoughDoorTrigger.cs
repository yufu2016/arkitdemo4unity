﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrhoughDoorTrigger : MonoBehaviour {
	public enum ThroughDoorState
	{
		StateAR = 0,
		StateVR
	}

	private ThroughDoorState currentState = ThroughDoorState.StateAR;

	public  SwitchState stateChanger; 

	void OnTriggerEnter(Collider other) {
		Debug.Log ("Enter"+other.gameObject.name);
		switch (currentState) {
		case ThroughDoorState.StateAR:
			stateChanger.SwitchStateNow (ThroughDoorState.StateVR);
			currentState = ThroughDoorState.StateVR;
			break;
		case ThroughDoorState.StateVR:
			stateChanger.SwitchStateNow (ThroughDoorState.StateAR);
			currentState = ThroughDoorState.StateAR;
			break;
		}
	}
}
