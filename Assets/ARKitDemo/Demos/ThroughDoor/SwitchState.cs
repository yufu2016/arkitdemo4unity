﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwitchState : MonoBehaviour {
	public RenderTexture renderCamera;
	public Camera arCamera;
	public Camera vrCamera;
	public LayerMask arLayerMask_Out;
	public LayerMask arLayerMask_In;

	public LayerMask vrLayerMask_Out;
	public LayerMask vrLayerMask_In;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SwitchStateNow(TrhoughDoorTrigger.ThroughDoorState state){
		switch (state) {
		case TrhoughDoorTrigger.ThroughDoorState.StateAR:
			arCamera.targetTexture = null;
			arCamera.cullingMask = arLayerMask_Out;
			vrCamera.targetTexture = renderCamera;
			vrCamera.cullingMask = vrLayerMask_In;
			break;
		case TrhoughDoorTrigger.ThroughDoorState.StateVR:
			arCamera.targetTexture = renderCamera;
			arCamera.cullingMask = arLayerMask_In;

			vrCamera.targetTexture = null;
			vrCamera.cullingMask = vrLayerMask_Out;
			break;
		}
	}


}
