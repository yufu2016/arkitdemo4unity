﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

//HitTestManager主要的目的是将ARkit中的HitTest集成进来，提供基本的需求，比如1.屏幕点击返回Hittest的ARPoint，2.持续获取屏幕中央的扫描点和扫描物体等

public class HitTestManager : MonoBehaviour {
	//定义监听事件——屏幕中央的持续事件
	public Action<Vector3,Quaternion > OnScreenCenterScan;

	//定义监听事件——点击屏幕返回的事件
	public Action<Vector3,Quaternion > OnScreenHit;



	//单例模式，全场景可用
	private static HitTestManager s_Instance;
	public static HitTestManager Instance
	{
		get
		{
			if (s_Instance == null)
			{
				s_Instance = FindObjectOfType<HitTestManager> ();
				DontDestroyOnLoad (s_Instance.gameObject);
			}

			return s_Instance;
		}
	}

	private void Awake ()
	{
		if (s_Instance == null)
		{
			s_Instance = this;
			DontDestroyOnLoad (this);
		}
		else if (this != s_Instance)
		{
			Destroy (gameObject);
		}
	}
		

	// Update is called once per frame
	void Update () {

		if (Input.touchCount > 0)
		{
			Debug.Log ("点击了。。。");
			var touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Began)
			{
				var screenPosition = Camera.main.ScreenToViewportPoint(touch.position);
				ARPoint point = new ARPoint {
					x = screenPosition.x,
					y = screenPosition.y
				};

				// prioritize reults types
				ARHitTestResultType[] resultTypes = {
					ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent, 
					// if you want to use infinite planes use this:
					//ARHitTestResultType.ARHitTestResultTypeExistingPlane,
					ARHitTestResultType.ARHitTestResultTypeHorizontalPlane, 
					ARHitTestResultType.ARHitTestResultTypeFeaturePoint
				}; 

				foreach (ARHitTestResultType resultType in resultTypes)
				{
					if (HitTestOnScreenResultType (point, resultType))
					{
						return;
					}
				}
			}
		}

		//如果没有需要就不做这些事情
		if (OnScreenCenterScan != null) {
			//对屏幕中心点的持续试探
			var screenCenterPosition = Camera.main.ScreenToViewportPoint (new Vector3 (Screen.width / 2, Screen.height / 2, 0));
			ARPoint centerPoint = new ARPoint {
				x = screenCenterPosition.x,
				y = screenCenterPosition.y
			};

			// prioritize reults types
			ARHitTestResultType[] resultCenterTypes = {
				ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent, 
				// if you want to use infinite planes use this:
				//ARHitTestResultType.ARHitTestResultTypeExistingPlane,
				ARHitTestResultType.ARHitTestResultTypeHorizontalPlane, 
				ARHitTestResultType.ARHitTestResultTypeFeaturePoint
			}; 


			foreach (ARHitTestResultType resultType in resultCenterTypes) {
				if (HitTestWithResultType (centerPoint, resultType)) {
					return;
				}
			}
		}

	}


	//发射屏幕中央的HitTest
	bool HitTestWithResultType (ARPoint point, ARHitTestResultType resultTypes)
	{
		List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface ().HitTest (point, resultTypes);
		if (hitResults.Count > 0) {
			foreach (var hitResult in hitResults) {
				//获取基于Unity坐标系统的位置和旋转，并发送出去
				var position = UnityARMatrixOps.GetPosition (hitResult.worldTransform);
				var rotation = UnityARMatrixOps.GetRotation (hitResult.worldTransform);
				if (OnScreenCenterScan != null) {
					OnScreenCenterScan (position, rotation);
				}
				return true;
			}
		}
		return false;
	}

	//发射点击屏幕的打击
	bool HitTestOnScreenResultType (ARPoint point, ARHitTestResultType resultTypes){
		Debug.Log ("出发了。。。");
		List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface ().HitTest (point, resultTypes);
		if (hitResults.Count > 0) {
			foreach (var hitResult in hitResults) {
				//获取基于Unity坐标系统的位置和旋转，并发送出去
				var position = UnityARMatrixOps.GetPosition (hitResult.worldTransform);
				var rotation = UnityARMatrixOps.GetRotation (hitResult.worldTransform);
				Debug.Log ("发射了。。。");
				if (OnScreenCenterScan != null) {
					OnScreenHit (position, rotation);
				}
				return true;
			}
		}
		return false;
	}
}
