# 说明文字如下 #

这是一个集结ARKit的Demo的地方，如果你有可以展示的Demo，欢迎提交到这里。这里也是一起研究ARKitAPI的地方。

### 目前的Demo ###

* Focus Square，一个展示当前平面位置的Demo（✅）
* ARMeasure，使用ARKit作为尺子（✅）
* ARAnchorEventDemo，锚点事件的使用（✅）
* ThroughDoor，穿越门效果(虚拟)（✅）
* ARMissile，导弹追踪ARCamera（✅）
* PortalAR，穿越门效果(真实)（不可行走✅）效果如图：
![alt text](https://bytebucket.org/yufu2016/arkitdemo4unity/raw/74eaf7e8aff9d863c5e706520da64d27b502abb7/Assets/ARKitDemo/Demos/PortalAR_Yufu/WX20170726-094058%402x.png)

* GeneralPlanDemo，产生平面和利用平面的Demo（开发中）
* 锚点与相机交互（开发中）
* 利用扫描的Point 动态生成Mesh（开发中）

### 开发版本需求 ###
* Unity3D : 2017.1
* Xcode 9.0 beta 3

### 我可以参与吗? ###

* 当然可以，请扫一扫加入群，提交你的SSH Key即可。
* 公钥生成方法：本地有的话去目录：
* cd ~/.ssh查看
* 没有的话生成一个：ssh-keygen
* 打印出来生成的Key ：cat ~/.ssh/你生成的名称.pub
* 把pub文件发给我也行。加我微信也行，微信号：vzheng2013
### 可以随意使用脚本吗？ ###

* 本项目原则上可以在任何项目中使用
